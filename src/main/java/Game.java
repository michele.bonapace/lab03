import java.util.ArrayList;
import java.util.List;

public class Game {

    List<Integer> rolls1;
    List<Integer> rolls2;

    public Game(){
        this.rolls1 = new ArrayList<>(0);
        this.rolls2 = new ArrayList<>(0);
    }

    public void roll(int pins){
        if (this.rolls1.size() == this.rolls2.size()){
            this.rolls1.add(pins);
            if(pins == 10)
                rolls2.add(0);
        }
        else
            this.rolls2.add(pins);
    }

    public int score(){
        int score = 0;

        for(int i = 0; i < rolls1.size() && i < 10; i++){
            if (this.rolls1.get(i) == 10){
                score += 10;
                int count = i + 1;
                if(count < this.rolls1.size() && this.rolls1.get(count) == 10){
                    score += 10;
                    count++;
                    if(count < this.rolls1.size())
                        score += this.rolls1.get(count);
                }
                else if(count < this.rolls1.size()){
                    score += this.rolls2.get(count) +  this.rolls1.get(count);
                }
            }
            else if(this.rolls1.get(i) + this.rolls2.get(i) == 10){
                score += 10;
                if (i + 1 < this.rolls1.size()){
                    score += this.rolls1.get(i + 1);
                }
            }
            else{
                score += this.rolls2.get(i) +  this.rolls1.get(i);
            }
        }

        return score;
    }
}
